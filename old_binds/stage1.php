<?php
error_reporting (E_ALL);

/*
$person = array ('name' => 'Dima', 'edge' => 22, 'gender' => 'M');

foreach ($person as $key => $val) {
	echo $key . ': ' . $val . "<br>"; 
}
*/
 /*
$person = array ('edge' => 22, 'gender' => 'M', 'name' => array ('first' => 'Dima', 'last' => 'Gaponenko'));

foreach ($person as $key => $val) {
	if (is_array($val)) {
		foreach ($val as $key1 => $num) {
			echo $key1 . ': ' . $num . "<br>";
		}
		continue;
	}
	echo $key . ': ' . $val . "<br>"; 
}
*/

/*
function display($key, $val) {
	return $key . ': ' . $val . "<br>";
}

$person = array ('edge' => 22, 'gender' => 'M', 'name' => array ('first' => 'Dima', 'last' => 'Gaponenko'));

foreach ($person as $key => $val) {
	if (is_array($val)) {
		foreach ($val as $key1 => $num) {
			echo display($key1, $num);
		}
		continue;
	}
	echo display($key, $val); 
}
*/

$more = function($a, $b) {
	return $a>$b;
};

	
$less = function($a, $b) {
	return $a<$b;
};

 function filter_arr($arr, $krit, $func) {
	$arr_5 = array ();
	foreach ($arr as $value) {
		if ($func($value, $krit)){
			$arr_5[] = $value;
		}
	}
	return $arr_5;

}

$arr = array (1, 2, 3, 4, 5, 6, 7, 8);

var_dump (filter_arr($arr, 5, function($a, $b) {
	return $a>$b;
}));

echo "<br>";

var_dump (filter_arr($arr, 5, function($a) {
	if ($a%2 <> 0){
		return $a;
	}
}));

echo "<br>";

var_dump (filter_arr($arr, 5, function($a) {
	if ($a%2 == 0){
		return $a;
	}
}));

echo "<br>";

$arr = array('fruit' => 'apple', 'veggie' => 'carrot');

// Верно
print $arr['fruit'];  echo "<br>";// apple
print $arr['veggie']; echo "<br>";// carrot

echo "<pre>";
print_r ($arr);
echo "</pre>";
var_dump($arr);

define('fruit', 'veggie');

// Теперь обратите внимание на разницу
print $arr['fruit'];  echo "<br>";// apple
print $arr[fruit];    echo "<br>";// carrot

echo "<pre>";
print_r ($arr);
echo "</pre>";
var_dump($arr);

?>