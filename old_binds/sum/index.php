<title>Сумма цифр числа</title>
<meta charset="UTF-8">

<?php

include_once '../functions.php';

if (isset($_POST['num'])) {
    $sum = sum($_POST['num']);
    echo "Сумма цифр числа " . $_POST['num'] . " = " . $sum;
}

?>


<form method="post" action="">
    Введите число (<=99999):
    <input type="number" name="num" max="99999" step="any"> <br /> <br />
    <button type="submit"> Найти сумму цифр числа</button>
    <button type="submit" formaction="../index.html">Перейти назад</button>
    <input type="reset" value="Очитстить поле"> <br /><br />
</form>