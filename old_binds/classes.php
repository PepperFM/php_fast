<?php

//require __DIR__ . '/../../vendor/autoload.php';

$students_data = [
    ['name' => ['first' => 'Ivan', 'second' => 'Ivanov'], 'age' => 15, 'gender' => 'm', 'class' => '5a'],
    ['name' => ['first' => 'Petr', 'second' => 'Petrov'], 'age' => 16, 'gender' => 'm', 'class' => '5b'],
    ['name' => ['first' => 'Masha', 'second' => 'Ivanova'], 'age' => 15, 'gender' => 'f', 'class' => '5a']
];

$prepods_data = [
    ['name' => ['first' => 'Ivan1', 'second' => 'Ivanov2', 'third' => 'Ivanovitch'], 'age' => 40, 'gender' => 'm', 'staj' => '10',
        'predmet'
    => 'math'],
    ['name' => ['first' => 'Petr2', 'second' => 'Petrov2', 'third' => 'Petrovitch'], 'age' => 45, 'gender' => 'm', 'staj' => '20', 'predmet'
    => 'chemistry']
];

class Man
{
    public $firstname;
    public $secondname;
    public $age;
    public $gender;

    public function __construct($data)
    {
        $this->age = $data['age'];
        $this->gender = $data['gender'];
        $this->firstname = $data['name']['first'];
        $this->secondname = $data['name']['second'];
    }

    public function getFullName()
    {
        return $this->firstname . ' ' . $this->secondname;
    }

    public function getAgeInDays()
    {
        return $this->age * 365;
    }
}

class Prepod extends Man
{
    public $staj;
    public $predmet;
    public $thirdname;

    public function __construct($data)
    {
        parent::__construct($data);
        $this->thirdname = $data['name']['third'];
        $this->staj = $data['staj'];
        $this->predmet = $data['predmet'];
    }

    public function getFullName()
    {
        return $this->firstname . ' ' . $this->secondname . ' ' . $this->thirdname;
    }
}

class Student extends Man
{
    public $class;

    public function __construct($data)
    {
        parent::__construct($data);
        $this->class = $data['class'];
    }
}

class Vypusknik extends Student
{
    public $year;
}

$students = [];

foreach ($students_data as $man) {
    $students[] = new Student($man);
}

//dump($students);

$prepods = [];
foreach ($prepods_data as $man) {
    $prepods[] = new Prepod($man);;
}

//dump($prepods);

foreach ($students as $man) {
    echo $man->getFullName() . ' ' . $man->age  . ' - ' . $man->getAgeInDays() . ' ' . $man->class . '<br>';
}

echo '<hr>';

foreach ($prepods as $man) {
    echo $man->getFullName() . ' ' . $man->age  . ' - ' . $man->getAgeInDays() . ' ' . $man->staj  . ' ' .
        $man->predmet . '<br>';
}














