<?php
// error_reporting(-1);
/**
 * Created by PhpStorm.
 * User: Damon
 * Date: 17.12.2017
 * Time: 19:42
 */

include_once "Strength.php";
include_once "Agility.php";
include_once "Intelligence.php";

$hero1 = new Agility(30, 2.4, 54, 58, 'eblan'); //17
$hero2 = new Strength(35, 3.2, 54, 66, 'axe'); //20
$hero3 = new Intelligence(30, 2,2, 58, 62); //14

echo "<pre>";
print_r($hero2);
echo "</pre>";

while ($hero1->health > 0 or $hero2->health > 0) {

    $hero1->incomingDmg($hero2->attack($hero2->minAtk, $hero2->maxAtk));
    echo "{$hero2->name} нанёс {$hero2->dmg} <br>";
    echo "у {$hero1->name} осталось {$hero1->health} <br><br>";

    if ($hero1->health < 0) {
        echo "{$hero1->name} проиграл";
        break;
    }

    $hero2->incomingDmg($hero1->attack());
    echo "{$hero1->name} нанёс {$hero1->dmg} <br>";
    echo "у {$hero2->name} осталось {$hero2->health} <br><br>";

    if ($hero2->health <0) {
        echo "{$hero2->name} проиграл";
        break;
    }
}
