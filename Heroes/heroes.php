<?php
/**
 * Created by PhpStorm.
 * User: Damon
 * Date: 17.12.2017
 * Time: 13:29
 */

class Heroes
{
    /**
    * @var integer ХП
    */
    public $health;

    /**
    * @var integer минимальное значение атаки
    */
    public $minAtk;

    /**
    * @var integer максимальное значение атаки
    */
    public $maxAtk;

    /**
    * @var integer урон: рандомное значение между минимальным и максимальным значением атаки
    */
    public $dmg;

    /**
    * @var string имя персонажа
    */
    public $name;

    /**
    * @var float количество силы за уровень
    */
    public $deltaStrength;

    /**
    * @var integer количество ХП за единицу силы
    */
    public $hpPerPoint;

    /**
    * @var integer ну левел, хули
    */
    private $level = 1;

    /**
     * @var integer Cила на первом уровне
     */
    public $baseStrength;


    public function __construct(int $baseStrength, float $deltaStrength, int $minAtk, int $maxAtk, $name = FALSE)
    {
        //$this->health = $health;
        $this->minAtk = $minAtk;
        $this->maxAtk = $maxAtk;
        $this->deltaStrength = $deltaStrength;
        $this->baseStrength = $baseStrength;
        $this->name = $name;

        $this->baseHealth();
        //$this->hpPerPoint = $hpPerPoint;
    }


    /**
    * Метод возвращает случайное число в диапазоне между мин. и макс. значением атаки
    *
    * @param integer $minAtk, $maxAtk -- минимальное и максимальное значение атаки
    * @return dmg
    */
    public function attack()
    {
        return $this->dmg = rand($this->minAtk, $this->maxAtk);
    }

    /**
    * Метод рассчитывает, как изменяется ХП в зависимости от нанесённого-входящего урона
    *
    * @param integer $health -- количество ХП, $dmg -- входящий урон
    */
    public function incomingDmg($dmg)
    {
        $this->health -= $dmg;
    }

    /**
    * Метод рассчитывает количество ХП, базируясь на силе, уровне и $deltaStrength
    *
    * @param integer $health
    */
    public function baseHealth()
    {
      $this->health = ($this->baseStrength + $this->level * $this->deltaStrength) * $this->hpPerPoint;
    }

    /**
     * Метод, изменяющий текущий уровень на введённый
     *
     * @param integer $level
     */
    // public function setLevel($level)
    // {
    //   $this->level = $level;
    // }

}
