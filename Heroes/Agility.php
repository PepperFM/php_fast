<?php
/**
 * Created by PhpStorm.
 * User: Damon
 * Date: 17.12.2017
 * Time: 14:21
 */

include_once "heroes.php";

class Agility extends Heroes
{
  public function __construct($baseStrength, $deltaStrength, $minAtk, $maxAtk, $name, $hpPerPoint = 17)
  {
        $this->hpPerPoint = $hpPerPoint;
        return parent::__construct($baseStrength, $deltaStrength, $minAtk, $maxAtk, $name);
  }
}
