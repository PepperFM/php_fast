<?php
/**
 * Created by PhpStorm.
 * User: Damon
 * Date: 17.12.2017
 * Time: 14:20
 */

include_once "heroes.php";

class Strength extends Heroes
{
    public function __construct($baseStrength, $deltaStrength, $minAtk, $maxAtk, $name, $hpPerPoint = 20)
    {
          $this->hpPerPoint = $hpPerPoint;
          return parent::__construct($baseStrength, $deltaStrength, $minAtk, $maxAtk, $name);
    }
}
