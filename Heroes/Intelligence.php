<?php
/**
 * Created by PhpStorm.
 * User: Damon
 * Date: 17.12.2017
 * Time: 14:21
 */

include_once "heroes.php";

class Intelligence extends Heroes
{
  public function __construct($baseStrength, $deltaStrength, $minAtk, $maxAtk, $hpPerPoint = 14)
  {
        $this->hpPerPoint = $hpPerPoint;
        return parent::__construct($baseStrength, $deltaStrength, $minAtk, $maxAtk);
  }
}
