<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28.11.2017
 * Time: 20:58
 */

final class InitSimple {

    public function get() {

        $link = mysqli_connect('127.0.0.1', 'root', '', 'new_test');
        mysqli_set_charset($link, 'utf8');
        $str = "SELECT * FROM test WHERE `result` IN ('success', 'normal')";

        $resultArray = [];
        $result = mysqli_query($link, $str);
        if (mysqli_error($link)) {
            print_r(mysqli_error($link));
        }

        while ($row = mysqli_fetch_assoc($result)) {
            $resultArray[] = $row;
        }

        return $resultArray;

    }

    private function create() {
        $link = mysqli_connect('127.0.0.1', 'root', '', 'new_test');
        mysqli_set_charset($link, 'utf8');

        $str = "CREATE TABLE test (id INT AUTO_INCREMENT,script_name varchar(25),start_time INT,end_time INT,result ENUM ('normal','illegal','failed','success'), PRIMARY KEY(id));";

        mysqli_query($link, $str);
        if (mysqli_error($link)) {
            print_r(mysqli_error($link));
        }
    }

    private function fill() {
        $arr = ['normal', 'illegal', 'failed', 'success'];

        $link = mysqli_connect('127.0.0.1', 'root', '', 'new_test');
        mysqli_set_charset($link, 'utf8');

        $script_name = $this->randString();
        $start_time = rand();
        $end_time = rand();
        $result = $arr[rand(0, 3)];

        $str = "INSERT INTO `test` (script_name,start_time,end_time,result) VALUES ('{$script_name}','{$start_time}','{$end_time}','{$result}')";

        mysqli_query($link, $str);
        if (mysqli_error($link)) {
            print_r(mysqli_error($link));
        }
    }

    private function randString($len = 25) {
        $abc = "QWERTYUPASDFGHJKLZXCVBNMqwertyuopasdfghjkzxcvbnm123456789";
        $str = str_shuffle($abc);
        return mb_substr($str, 0, $len);
    }

}