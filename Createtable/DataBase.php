<?php

class EmptyNameExeption extends Exception
{
  //protected $message = 'Не введено имя пользователя';   // сообщение об исключении

  public function __construct($message = null, $code = 0, Exception $previous = null) {
    parent::__construct($message, $code, $previous);

    if ($message == NULL) {
    $this->message = "Не введено имя пользователя";
  }
    file_put_contents('file.txt', $this->message.PHP_EOL, FILE_APPEND);
  }
}

class EmptyPasswordExeption extends Exception
{
  protected $message = 'Не введён пароль';   // сообщение об исключении
}

class EmptyOtherExeption extends Exception
{
  protected $message = 'Введите данные в поля';   // сообщение об исключении
}


class DataBase
{
    /**
     * @var string Хост подключения
     */
    protected $host;

    /**
     * @var string Логин
     */
    protected $login;

    /**
     * @var string Пароль к бд
     */
    protected $pass;

    /**
     * @var mixed|string Имя базы данных
     */
    protected $BD;

    /**
     * @var resourse Поток подключения к бд
     */
    protected $link;

    /**
     * @var string Строка запроса к бд
     */
    protected  $stringQuery = '';

    function __construct($connect = [])
    {
        if (isset($connect['login']) && $connect['login']) {
          $this->login = $connect['login'];
        } else {
          throw new EmptyNameExeption();
        }

        if (isset($connect['pass']) && $connect['pass']) {
          $this->login = $connect['pass'];
        } else {
          throw new EmptyPasswordExeption();
        }


        $this->host = $connect['host'] ?? '127.0.0.1';
//        $this->login = $connect['login'] ?? 'root';
//        $this->pass = $connect['pass'] ?? '';
        $this->BD = $connect['BD'] ?? 'main';
    }

    /**
     * Подключение к бд с текущими параметрами
     *
     * @return init
     * @throws Exception
     */
    public function connect(): DataBase
    {
        $this->link = @mysqli_connect($this->host, $this->login, $this->pass, $this->BD);

//        var_dump(mysqli_connect_error($this->link));

        if(!$this->link){
            throw new EmptyOtherExeption();
        }

        return $this;
    }

    /**
     * Установка кодировки
     *
     * @param string $charset
     * @return init
     */
    public function setCharset(string $charset = 'utf8'): DataBase
    {
        mysqli_set_charset($this->link, $charset);
        return $this;
    }

    /**
     * Метод создает начало строки запроса
     *
     * @param string $tableName Название таблицы из которой делается выборка
     * @return DataBase
     */
    public function find($tableName): DataBase
    {
        $this->stringQuery = "SELECT * FROM {$tableName} ";
        return $this;
    }

    /**
     * Метод задает параметры выборки из бд.
     *
     * @param $array Массив, где ключ - название поля по которому происходит выборка,
     * а значение - либо массив, тогда формируется строкка where id, или если не массив то просто where
     * @return DataBase
     */
    public function where($array): DataBase
    {
        $key = key($array);
        if (is_array($array[$key])) {
            $this->stringQuery .= "WHERE `$key` IN (";

            foreach ($array[$key] as $item) {
                $this->stringQuery .= "'{$item}', ";
            }
            $this->stringQuery = mb_substr($this->stringQuery, 0, strlen($this->stringQuery) - 2);
            $this->stringQuery .= ")";
        } else {
            $this->stringQuery .= "WHERE `$key` = '{$array[$key]}'";
        }

        return $this;
    }

    /**
     * Метод производит запрос к бд и формирует массив, в котором ключами являются названия полей, а значениями
     * значения полей
     *
     * @return array Результирующий массив
     */
    public function get(): array
    {
        $resultArray = [];
        $result = mysqli_query($this->link, $this->stringQuery);
        if (mysqli_error($this->link)) {
            print_r(mysqli_error($this->link));
        }

        while ($row = mysqli_fetch_assoc($result)) {
            $resultArray[] = $row;
        }

        return $resultArray;
    }

    /**
     * Создание таблицы
     *
     *
     * @param string $tableName Название таблицы
     * @param array $fields Массив с полями таблицы в формате
     * ```php
     * [
     *      'id' => 'INT', // Названием поля таблицы служит ключ массива, а типом поля значение
     *      'name' => 'string',
     * ]
     * ```php
     * @param string $primaryKey Первичный ключ
     * @return bool|string
     */
    public function createTable(string $tableName, array $fields, string $primaryKey = 'id')
    {
        $i = 0;
        $table = "CREATE TABLE {$tableName} (";
        foreach ($fields as $key => $field) {
            $table .= $key . " " . $field . ",";

            if (++$i == count($fields)) {
                $table .= " PRIMARY KEY({$primaryKey})";
            }
        }
        $table .= ');';

        mysqli_query($this->link, $table);
        if (mysqli_error($this->link)) {
            return mysqli_error($this->link);
        }

        return true;
    }

    /**
     * Выполнение пользовательского запроса
     *
     * @param string $query Текст запрса
     * @return bool|string
     */
    public function query(string $query)
    {
        mysqli_query($this->link, $query);
        if (mysqli_error($this->link)) {
            return mysqli_error($this->link);
        }

        return true;
    }

    public function insert(string $tableName, array $values)
    {

        $i = 0;
        $stroka = "INSERT INTO `{$tableName}` (";

        foreach ($values as $key => $value) {
            if (++$i == count($values)) {
                $stroka .= "{$key}";
                continue;
            }
            $stroka .= "{$key},";
        }

        $stroka .= ") VALUES (";

        $i = 0;
        foreach ($values as $key => $value) {
            if (++$i == count($values)) {
                $stroka .= "'{$value}'";
                continue;
            }
            $stroka .= "'{$value}',";
        }

        $stroka .= ")";

        mysqli_query($this->link, $stroka);
        if (mysqli_error($this->link)) {
            return mysqli_error($this->link);
        }

        return true;
    }

    public function updateTable (string $tableName, array $values)
    {
        $stroka = "UPDATE `{$tableName}` SET ";

        foreach ($values as $key => $value) {
            $stroka .= "{$key}" . "=" . "{$value}, ";
        }
    }

    public function dropTable (string  $tableName)
    {
        $stroka = "DROP TABLE {$tableName}";
        mysqli_query($this->link, $stroka);
    }

}
