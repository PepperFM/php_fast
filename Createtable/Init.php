<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28.11.2017
 * Time: 20:12
 */
require_once 'DataBase.php';

final class Init {

    /**
     * @var Класс для подключения к бд
     */
    public $db;
    protected $db_name;
    protected $table_name;

    public function __construct($db_name, $db_table)
    {
        $this->db_name = $db_name;
        $this->table_name = $db_table;
        try {
            $this->db = (new DataBase([
                'login' => '',
                'pass' => '12',
                'BD' => $this->db_name,
            ]))->connect()->setCharset();

        } catch (Exception $e) {
            echo "<div class='text-align:center; color: red; '>{$e->getMessage()}</div>";
            exit();
        }


        $this->create();
        //$this->fill();
    }

    private function create() {
       $this->db->createTable($this->table_name, [
            'id' => 'INT AUTO_INCREMENT',
            'script_name' => 'varchar(25)',
            'start_time' => 'INT',
            'end_time' => 'INT',
            'result' => "ENUM ('normal','illegal','failed','success')"
        ]);
    }

    private function fill() {
        $arr = ['normal', 'illegal', 'failed', 'success'];

        for ($i=0; $i<100; $i++){
            $this->db->insert($this->table_name, [
                'script_name' => $this->randString(),
                'start_time' => rand(),
                'end_time' => rand(),
                'result' => $arr[rand(0, 3)],
            ]);
        }


    }

    private function randString($len = 25) {
        $abc = "QWERTYUPASDFGHJKLZXCVBNMqwertyuopasdfghjkzxcvbnm123456789";
        $str = str_shuffle($abc);
        return mb_substr($str, 0, $len);
    }

    public function get() {
        return $this->db->find($this->table_name)->where(['result' => ['success', 'normal']])->get();
    }

    public function drop(){
        return $this->db->dropTable($this->table_name);
    }
}
